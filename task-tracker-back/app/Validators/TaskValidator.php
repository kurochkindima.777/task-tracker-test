<?php

namespace App\Validators;

use Illuminate\Support\Facades\Validator;

class TaskValidator
{
    public function validate(array $data)
    {
        $validator = Validator::make($data, [
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:5000',
            'due_date' => 'required|date|after_or_equal:today',
            'status' => 'nullable|in:added,in_progress,completed',
            'user_name' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            throw new \InvalidArgumentException($validator->errors()->first());
        }
    }
}
