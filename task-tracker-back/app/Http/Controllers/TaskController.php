<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use App\Validators\TaskValidator;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $tasks = Task::all();

        return $tasks;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function create(Request $request)
    {
        $validator = new TaskValidator();
        try {
            $validator->validate($request->all());
            Task::create($request->all());
            return Task::all();
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $validator = new TaskValidator();
        $validator->validate($request->all());

        $task = Task::find($id);
        if (!$task) {
            return response()->json(['error' => 'Task not found'], 404);
        }

        $data = $request->all();
        // Если задача помечается как завершенная, установите текущую дату в completed_date
        if ($data['status'] === 'completed') {
            $data['completed_date'] = \Carbon\Carbon::now()->toDateTimeString();
        }

        $task->update($data);

        return $task;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        Task::find($id)->delete();
    }
}
