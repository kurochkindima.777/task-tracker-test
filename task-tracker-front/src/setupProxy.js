const {createProxyMiddleware} = require('http-proxy-middleware');

module.exports = function (app) {
    app.use(
        '/api',
        createProxyMiddleware({
            // у меня не завелся на localhost, пришлось импровизировать
            target: 'http://todohost:8000', // адрес backend-сервера
            changeOrigin: true,
        })
    );
};