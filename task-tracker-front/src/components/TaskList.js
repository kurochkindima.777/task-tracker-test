import React, {useEffect, useState} from 'react';
import TaskItem from './TaskItem';

function TaskList({onStatusChange, onDelete}) {
    const [tasks, setTasks] = useState([]);
    const [sortField, setSortField] = useState('');
    const [sortOrder, setSortOrder] = useState('asc');

    useEffect(() => {
        fetch('/api/tasks')
            .then(response => response.json())
            .then(data => setTasks(data));
    }, [setTasks]);

    const handleSort = (field) => {
        if (field === sortField) {
            setSortOrder(sortOrder === 'asc' ? 'desc' : 'asc');
        } else {
            setSortField(field);
            setSortOrder('asc');
        }
    };

    const sortedTasks = tasks.slice().sort((a, b) => {
        let comparison = 0;

        if (sortField === 'user_name') {
            comparison = a.user_name.localeCompare(b.user_name);
        } else if (sortField === 'status') {
            comparison = a.status.localeCompare(b.status);
        } else if (sortField === 'due_date') {
            comparison = new Date(a.due_date) - new Date(b.due_date);
        } else if (sortField === 'completed_date') {
            comparison = new Date(a.completed_date) - new Date(b.completed_date);
        }

        return sortOrder === 'asc' ? comparison : -comparison;
    });

    return (
        <div>
            <table>
                <thead>
                <tr>
                    <th>Название задачи</th>
                    <th className="sort_task" onClick={() => handleSort('user_name')}>Имя исполнителя</th>
                    <th>Описание задачи</th>
                    <th className="sort_task" onClick={() => handleSort('due_date')}>Срок выполнения</th>
                    <th className="sort_task" onClick={() => handleSort('completed_date')}>Дата выполнения</th>
                    <th className="sort_task" onClick={() => handleSort('status')}>Статус задачи</th>
                </tr>
                </thead>
                <tbody>
                {sortedTasks.map(task => <TaskItem onStatusChange={onStatusChange} onDelete={onDelete} key={task.id}
                                                   task={task}/>)}
                </tbody>
            </table>
        </div>
    );
}

export default TaskList;
