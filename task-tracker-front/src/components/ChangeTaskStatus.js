import React from 'react';


function ChangeTaskStatus({task, onChange}) {
    const handleStatusChange = event => {
        const data = {...task, status: event.target.value};
        onChange(data);
    };

    const isCompleted = task.status === 'completed';

    return (
        <select value={task.status} onChange={handleStatusChange} disabled={isCompleted}>
            <option value="added">Добавлена</option>
            <option value="in_progress">В работе</option>
            <option value="completed">Завершена</option>
        </select>
    );
}

export default ChangeTaskStatus;
