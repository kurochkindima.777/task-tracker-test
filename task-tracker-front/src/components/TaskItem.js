import React from 'react';
import ChangeTaskStatus from './ChangeTaskStatus';
import DeleteTaskButton from './DeleteTaskButton';
import {format} from "date-fns";

function TaskItem({task, onStatusChange, onDelete}) {
    const {title, user_name, description, due_date, completed_date } = task;

    const handleStatusChange = data => {
        onStatusChange(data);
    };

    const handleDelete = data => {
        onDelete(data);
    };

    return (
        <tr className={due_date < new Date() ? 'overdue' : ''}>
            <td>{title}</td>
            <td>{user_name}</td>
            <td>{description}</td>
            <td>{format(new Date(due_date), 'dd.MM.yyyy')}</td>
            <td>{completed_date ? format(new Date(completed_date), 'dd.MM.yyyy') : ''}</td>
            <td>
                <ChangeTaskStatus task={task}  onChange={handleStatusChange}/>
                <DeleteTaskButton task={task} onDelete={handleDelete}/>
            </td>
        </tr>
    );
}

export default TaskItem;
