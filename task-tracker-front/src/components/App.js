import React, {useState, useEffect} from 'react';
import TaskList from './TaskList';
import CreateTaskForm from './CreateTaskForm';

function App() {
    const [tasks, setTasks] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState('');

    useEffect(() => {
        async function fetchData() {
            await fetchTasks();
        }

        fetchData()
    }, []);

    function fetchTasks() {
        setLoading(true);
        return fetch('/api/tasks')
            .then(response => response.json())
            .then(data => {
                setTasks(prevTasks => [...prevTasks, ...data]);
                setLoading(false);
            });
    }

    const handleCreateTask = data => {
        const requestOptions = {
            method: 'PUT',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(data)
        };

        fetch('/api/tasks', requestOptions)
            .then(response => {
                if (!response.ok) {
                    return response.json().then(data => {
                        setError(data.message)
                    })
                }
                setError('');
                return fetchTasks();
            })
            .catch(error => {
                console.log(error);
                setError(error.message);
            });
    };

    const handleStatusChange = data => {
        patchTask(data).then(() => fetchTasks());
    };

    function patchTask(data) {
        const requestOptions = {
            method: 'PATCH',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(data)
        };

        return fetch('/api/tasks/' + data.id, requestOptions)
            .then(response => response.json())
            .catch(error => console.error(error));
    }

    const handleDelete = data => {
        const requestOptions = {
            method: 'DELETE',
            headers: {'Content-Type': 'application/json'},
        };

        return fetch('/api/tasks/' + data.id, requestOptions)
            .then(() => fetchTasks())
            .catch(error => console.error(error));
    };

    return (
        <div className="container">
            <h1>Создание задачи</h1>
            <CreateTaskForm createTasks={handleCreateTask} error={error}/>
            {!loading && tasks.length > 0 ? (
                <TaskList
                    tasks={tasks}
                    onStatusChange={handleStatusChange}
                    onDelete={handleDelete}
                    setTasks={setTasks}
                />
            ) : (
                <p>Нет задач для отображения</p>
            )}
        </div>
    );
}

export default App;
