import React from 'react';

function DeleteTaskButton({ task, onDelete }) {
    const handleClick = data => {
        onDelete(task);
    };

    return <button onClick={handleClick}>Удалить</button>;
}

export default DeleteTaskButton;
