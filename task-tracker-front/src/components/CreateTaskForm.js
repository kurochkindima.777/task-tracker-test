import React, {useState} from 'react';
import {format} from "date-fns";

function CreateTaskForm({createTasks, error}) {
    const [title, setTitle] = useState('');
    const [user_name, setUserName] = useState('');
    const [description, setDescription] = useState('');
    const [due_date, setDueDate] = useState(format(new Date(Date.now() + 24 * 60 * 60 * 1000), 'yyyy-MM-dd'));

    const handleSubmit = event => {
        event.preventDefault();
        const data = {title, user_name, description, due_date};
        createTasks(data);
    };

    return (
        <form onSubmit={handleSubmit}>
            <div>
                <label htmlFor="title">Название задачи:</label>
                <input
                    id="title"
                    type="text"
                    value={title}
                    onChange={event => setTitle(event.target.value)}
                />
            </div>
            <div>
                <label htmlFor="name">Имя исполнителя:</label>
                <select value={user_name} onChange={event => setUserName(event.target.value)}>
                    <option disabled value="">Не выбрано</option>
                    <option value="Дмитрий">Дмитрий</option>
                    <option value="Антон">Антон</option>
                    <option value="Павел">Павел</option>
                </select>
            </div>
            <div>
                <label htmlFor="description">Описание задачи:</label>
                <input
                    id="description"
                    type="text"
                    value={description}
                    onChange={event => setDescription(event.target.value)}
                />
            </div>
            <div>
                <label htmlFor="due_date">Срок выполнения:</label>
                <input
                    id="due_date"
                    type="date"
                    value={due_date}
                    onChange={event => setDueDate(event.target.value)}
                    min={format(new Date(), 'yyyy-dd-MM')}
                />
            </div>
            <button type="submit">Создать задачу</button>
            {error ? (<p style={{color: "red"}}>{error}</p>) : ''}
        </form>
    );
}

export default CreateTaskForm;
